# To deploy your own 7d2d server on fly.io

## 1. Install flyctl
Go through steps 1. 2. 3. in https://fly.io/docs/hands-on/
to install flyctl and login.

7d2d requires performance machine, so no free tier,
most usages are calculated hourly or per second,
the bills come monthly.

## 2. Create the app
```shell
flyctl launch \
--no-deploy \
--copy-config \
--dockerignore-from-gitignore \
--ha=false \
--yes
```

## 3. Create the volume.
Costs $0.15 / (gig * month)

This will hold both the game files and save files,
so if you want to randgen the world, increase the size to 40.

If you have changed the region of the app in `fly.toml:primary_region`,
make sure it matches the region parameter here
```shell
flyctl volume create 7d2d_data --size 20 --region arn --yes
```

## 4. Allocate a dedicated ipv4
Costs $2/month
```shell
flyctl ips allocate-v4 --yes
```

## 5. Edit the 7d2d [server config](./serverconfig.xml)

Reference the [docs](https://developer.valvesoftware.com/wiki/7_Days_to_Die_Dedicated_Server#Serverconfig.xml)

Committing the changes is advised but not required.
The local copy of the repo is sent to the fly.io builder when running the next command.

## 5. 2. (Optional) Set the password

```shell
flyctl secrets set "SDTD_SRV_PW=my super secret password"
```

## 6. Run the server
Costs $82.01/month. Usage calculated per second.

```shell
flyctl deploy --now
```

It will take
- ~5 minutes to build all images and deploy
- ~20 minutes to download the game
- (if chosen) ~20 minutes to generate the world
- ~5 minutes to load

## 7. Stop the server

After finished playing

```shell
flyctl machine stop $(flyctl machine list -q)
```

## 8. Restart the server
```shell
flyctl machine start $(flyctl machine list -q)
```

### Utils
ssh to the server
```shell
flyctl ssh console
```
read logs
```shell
flyctl logs
```
check status
```shell
flyctl status
```

## Backup

shell into the app, then
```shell
cd /data
tar -czvf backup.tar.gz UserData
```
