use std::collections::HashSet;
use std::ffi::CString;
use std::sync::{Mutex, MutexGuard, OnceLock};

use libc::c_int;

static UDP_SOCKETS_FDS: OnceLock<Mutex<HashSet<c_int>>> = OnceLock::new();

fn get_sockets<'a>() -> MutexGuard<'a, HashSet<c_int>> {
    UDP_SOCKETS_FDS
        .get_or_init(Default::default)
        .lock()
        .unwrap()
}

macro_rules! get_original_function {
    ($name:expr, $ty:ty) => {{
        let orig_name = CString::new($name).unwrap();

        let orig_fn_ptr = unsafe { libc::dlsym(libc::RTLD_NEXT, orig_name.into_raw()) };
        let f: $ty = unsafe { std::mem::transmute(orig_fn_ptr) };
        f
    }};
}

#[no_mangle]
pub extern "C" fn socket(domain: c_int, ty: c_int, protocol: c_int) -> c_int {
    let sock =
        get_original_function!("socket", fn(c_int, c_int, c_int) -> c_int)(domain, ty, protocol);

    if sock != -1 && domain == libc::AF_INET && ty == libc::SOCK_DGRAM {
        get_sockets().insert(sock);
    }

    sock
}

#[no_mangle]
pub extern "C" fn bind(
    sock: c_int,
    addr: *const libc::sockaddr,
    addr_len: libc::socklen_t,
) -> c_int {
    let mut addr_ptr = addr;
    let mut new_addr: libc::sockaddr;
    if get_sockets().contains(&sock) {
        new_addr = unsafe { *addr };
        let fly_dns_bind_addr = dns_lookup::lookup_host("fly-global-services")
            .expect("to resolve the address")
            .into_iter()
            .filter_map(|a| match a {
                std::net::IpAddr::V4(v4) => Some(v4.octets()),
                std::net::IpAddr::V6(_) => None,
            })
            .next()
            .unwrap();

        for (target, source) in (&mut new_addr.sa_data[2..6])
            .into_iter()
            .zip(fly_dns_bind_addr)
        {
            *target = source as i8;
        }
        addr_ptr = &new_addr;
    }

    get_original_function!(
        "bind",
        fn(c_int, *const libc::sockaddr, libc::socklen_t) -> c_int
    )(sock, addr_ptr, addr_len)
}
