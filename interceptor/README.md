# Bind interceptor

can be `LD_PRELOAD`ed into your app and will fix binding to UDP ports
in the fly.io context.

For example, running a netcat bound to a UDP port in a fly.io context:

```shell
cargo build --release
LD_PRELOAD=target/release/libinterceptor.so nc -l -u 0.0.0.0 2233
```

[More Details](https://samoylovfp.dev/configuring-inconfigurable/)
