# Deploying

```shell
fly launch --copy-config \
--no-deploy \
--dockerignore-from-gitignore \
--ha=false \
--yes
```

```shell
fly secrets set TELOXIDE_TOKEN=<token from botfather> --stage
# can first set to 0, send a message and read the chat_id from the log
fly secrets set SD_BOT_CTRL_CHAT=<chat id for control> --stage
# create token in the "tokens" directory of the fly.io dashboard
fly secrets set "FLY_TOKEN=<fly io token>" --stage
fly secrets set "FLY_7D_APP=<server app name, e.g. "7d2d-ded">"
```
