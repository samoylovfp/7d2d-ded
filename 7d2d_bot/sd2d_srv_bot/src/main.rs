use std::{fmt::Write, sync::OnceLock};

use reqwest::Client;
use serde::Deserialize;
use teloxide::{
    macros::BotCommands,
    prelude::*,
    update_listeners::webhooks::{axum_no_setup, Options},
    RequestError,
};

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    log::info!("Starting 7d2d management bot");

    let bot = Bot::from_env();

    if option_env!("LOCAL_DEV").is_some() {
        Command::repl(bot.clone(), handle_messages).await;
    } else {
        let address = "0.0.0.0:8080".parse().unwrap();
        let url: url::Url = "https://7d2d-bot.fly.dev".parse().unwrap();
        let (listener, _, router) = axum_no_setup(Options::new(address, url.clone()));

        tokio::spawn(async move {
            axum::Server::bind(&address)
                .serve(router.into_make_service())
                .await
                .expect("Axum server error");
        });
        bot.set_webhook(url).await.unwrap();
        Command::repl_with_listener(bot.clone(), handle_messages, listener).await;
    }
}

#[derive(BotCommands, Clone)]
#[command(rename_rule = "lowercase")]
enum Command {
    Start,
    Stop,
}

#[derive(Deserialize, Debug)]
struct FlyMachine {
    id: String,
    // only used for debug logging
    #[allow(unused)]
    state: String,
}

static CTRL_CHAT_ID: OnceLock<i64> = OnceLock::new();

async fn handle_messages(bot: Bot, msg: Message, cmd: Command) -> Result<(), RequestError> {
    let ctrl_chat =
        CTRL_CHAT_ID.get_or_init(|| std::env::var("SD_BOT_CTRL_CHAT").unwrap().parse().unwrap());
    if msg.chat.id.0 != *ctrl_chat {
        log::info!(
            "Message from non-control chat: {}, {:?}",
            msg.chat.id,
            msg.text().map(|t| &t[0..t.len().min(100)])
        );
        return Ok(());
    }
    let fly_token = std::env::var("FLY_TOKEN").unwrap();
    let auth = format!("Bearer {fly_token}");

    match cmd {
        Command::Start => {
            switch_server(
                "Запускаю сервер",
                "start",
                &auth,
                &bot,
                &msg,
                Some("Будет загружаться минут 10"),
            )
            .await?
        }
        Command::Stop => {
            switch_server("Останавливаю сервер...", "stop", &auth, &bot, &msg, None).await?
        }
    }

    Ok(())
}

async fn switch_server(
    init_message: &str,
    endpoint: &str,
    auth: &str,
    bot: &Bot,
    msg: &Message,
    post_msg: Option<&str>,
) -> Result<(), RequestError> {
    let appname = std::env::var("FLY_7D_APP").unwrap();
    let client = Client::new();
    let mut text = init_message.to_string();
    let msg = bot.send_message(msg.chat.id, &text).await?;
    let mach: Vec<FlyMachine> = client
        .get(format!("https://api.machines.dev/v1/apps/{appname}/machines?include_deleted=false"))
        .header("Authorization", auth)
        .send()
        .await?
        .json()
        .await?;

    _ = write!(&mut text, "\nМашины `{:?}`", mach);
    bot.edit_message_text(msg.chat.id, msg.id, &text)
        .await
        .unwrap();

    let Some(m) = mach.first() else { return Ok(()) };
    let result = client
        .post(format!(
            "https://api.machines.dev/v1/apps/{appname}/machines/{}/{endpoint}",
            m.id
        ))
        .header("Authorization", auth)
        .send()
        .await;
    _ = write!(&mut text, "\n Результат ");
    let res = match result {
        Err(e) => format!("{e:?}"),
        Ok(r) => {
            format!(
                "{} {:?}",
                r.status(),
                r.bytes()
                    .await
                    .map(|r| String::from_utf8_lossy(&r).to_string())
            )
        }
    };
    _ = write!(&mut text, "\n```{res}```");
    if let Some(ps) = post_msg {
        _ = write!(&mut text, "\n{ps}");
    }

    bot.edit_message_text(msg.chat.id, msg.id, &text)
        .await
        .unwrap();
    Ok(())
}
