#!/bin/bash

## Uncomment for debug
# JUST_SLEEP=1
if [ -n "$JUST_SLEEP" ]; then
  sleep infinity &
  wait $!
fi

_term() { 
  pkill 7DaysToDie
  pkill steamcmd
  pkill sleep
}

trap _term SIGINT

## Update the server
steamcmd +runscript /update_7d2d.steamcmd &
child=$!
# wait allows propagating signals to children
wait "$child"
# if steamcmd was killed, then exit
if [ "$?" != "0" ]; then exit 1; fi  

cd /data/7D2D

# set up password
if [ -n "$SDTD_SRV_PW" ]; then
  sed -i -E 's/name="ServerPassword"\s+value=.*"/name="ServerPassword" value="'$SDTD_SRV_PW'"/' /usr/local/serverconfig.xml
fi

LD_PRELOAD=/libinterceptor.so ./7DaysToDieServer.x86_64 -logfile ../log.txt -quit -batchmode -nographics -configfile=/usr/local/serverconfig.xml -dedicated &
child=$!
wait "$child"
