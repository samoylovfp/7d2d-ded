FROM docker.io/rust:1.75.0 as builder
COPY interceptor /usr/src/interceptor
WORKDIR /usr/src/interceptor
RUN cargo build --release

FROM docker.io/steamcmd/steamcmd:ubuntu-22
COPY update_7d2d.steamcmd /update_7d2d.steamcmd
COPY entrypoint.sh /entrypoint.sh
COPY --from=builder /usr/src/interceptor/target/release/libinterceptor.so /
COPY serverconfig.xml /usr/local/serverconfig.xml
ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]
